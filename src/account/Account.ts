import {IAccount} from "./IAccount";
import {BALANCE_ERRORS_TYPE, BalanceError} from "../balance/BalanceErrors";

export class Account implements IAccount{
    readonly ID: number;
    private _balance:number = 0;
    constructor(id:number) {
        this.ID = id;
    }

    addBalance(value: number): void {
        this._balance += value;
    }

    spend(value: number): void {
        if(this._balance < value){
            throw new BalanceError(BALANCE_ERRORS_TYPE.INSUFFICIENT_FUNDS, `Balance=${this._balance}`)
        }

        this._balance -= value;
    }

    getBalance(): number {
        return this._balance;
    }

    clear():void{
        this._balance = 0;
    }
}