import {IBalanceAction} from "../balance/IBalance";

export interface IAccount extends IBalanceAction{
    readonly ID:number;
    clear():void;
}