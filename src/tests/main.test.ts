import {IAccount} from "../account/IAccount";
import {AVAILABLE_PICK, COIN_SIDE, IAccountPick, IGame} from "../game/IGame";
import {Account} from "../account/Account";
import {Game} from "../game/Game";
import {IMathModel} from "../logic/MathTypes";
import {MathFiftyFiftyModel} from "../logic/models/MathFiftyFiftyModel";
import {StatisticModule} from "../statistic/StatisticModule";

async function delay(sec = 0.1) {
    await new Promise<void>((resolve, reject) => {
        setTimeout(function () {
            resolve()
        }, sec * 1000)
    })
}

describe("Game test", () => {
    const acc:IAccount = new Account(1);
    const game:IGame = new Game();
    const statistic:StatisticModule = StatisticModule.getInstance();

    beforeEach(() => {
        acc.addBalance(100);
        const mathModel:IMathModel = new MathFiftyFiftyModel();
        game.setMathModel(mathModel);
    })

    afterEach(async () => {
        game.clear();
        acc.clear();
        StatisticModule.getInstance().clear();
        await delay(2)
    })

    describe('Start', () => {
        it('play game', async () => {
            game.exec({acc:acc, bet:2, pick:COIN_SIDE.TAIL} as IAccountPick);
            expect(statistic.countGames).toBe(1);
        })
        it('play game2', async () => {
            game.exec({acc:acc, bet:2, pick:COIN_SIDE.TAIL} as IAccountPick);
            game.exec({acc:acc, bet:2, pick:COIN_SIDE.TAIL} as IAccountPick);
            game.exec({acc:acc, bet:2, pick:COIN_SIDE.TAIL} as IAccountPick);
            expect(statistic.countGames).toBe(3);
        })
        it('play game 10^6', async () => {
            acc.addBalance(10000000);
            const count:number = 1000000;
            for(let i = 0;i<count;i++){
                game.exec({acc:acc, bet:2, pick:COIN_SIDE.HEAD} as IAccountPick);
            }
            expect(statistic.countGames).toBe(count);
            StatisticModule.getInstance().printStatistic();
        })
    })


})