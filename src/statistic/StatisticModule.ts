import {IStepGame} from "../game/IGame";
import {WinResult} from "../logic/results/WinResult";
import {LoseResult} from "../logic/results/LoseResult";

export class StatisticModule {
    private static _instance:StatisticModule;

    private _totalBet:number = 0;
    private _totalWin:number = 0;
    private _countGames:number = 0;
    private _countWinGames:number = 0;
    private _countLoseGames:number = 0;

    private _steps:Array<IStepGame> = [];

    constructor() {
        if(StatisticModule._instance){
            throw new Error("Statistic module initialized")
        }
    }

    get countGames():number{
        return this._countGames;
    }

    static getInstance():StatisticModule{
        if(!StatisticModule._instance){
            StatisticModule._instance = new StatisticModule();
        }
        return StatisticModule._instance;
    }

    add(step:IStepGame, isSaveSteps:boolean = true):void{
        if(isSaveSteps){
            this._steps.push(step)
        }

        this._totalBet += step.bet;
        this._totalWin += step.result.getResult();

        if(step.result instanceof WinResult){
            this._countWinGames += 1;
        }else{
            this._countLoseGames += 1;
        }
        this._countGames += 1;
    }

    clear():void{
        this._countGames = 0;
        this._totalWin = 0;
        this._countLoseGames = 0;
        this._countWinGames = 0;
        this._totalBet = 0;

        //if steps is non basic object, shold call destroy
        this._steps = [];
    }

    printStatistic():void{
        console.log(`
            totalBet: ${this._totalBet}
            sumWins: ${this._totalWin};
            hitRate: ${this._countWinGames / this._countGames}
            avarageWins: ${this._totalWin/this._countGames};
            averagePositiveWin: ${this._totalWin/this._countWinGames}
        `);
    }

    getStatisticBySteps(steps:Array<IStepGame>){
        if(steps === null) return;

        const w = {
            total: 0,
            count: 0,
        };

        let l = {
            total: 0,
            count: 0,
        }
        const bet = steps[0].bet;
        const countGames:number = steps.length;

        steps.forEach((step:IStepGame) => {
            if(step.result instanceof WinResult){
                w.total += step.result.winValue;
                w.count += 1;
            }
            if(step.result instanceof LoseResult){
                l.count += 1;
            }
        })

        console.log(`
            totalBet: ${bet * countGames};
            sumWins: ${w.total};
            hitRate: ${w.count / countGames};
            avarageWins: ${w.total/countGames};
            averagePositiveWin: ${w.total/w.count}
        `);
    }
}