import {IAccount} from "../account/IAccount";
import {IMathModel, IResult} from "../logic/MathTypes";

export enum COIN_SIDE {
    HEAD="HEAD",
    TAIL="TAIL",
    EDGE="EDGE"
}

export type AVAILABLE_PICK = COIN_SIDE.TAIL | COIN_SIDE.HEAD;


export interface IGame {
    exec(data:IAccountPick):IResult;
    setMathModel(model:IMathModel):void;
    clear():void;
    log():void;
}

export type IBet = number;

export interface IAccountPick {
    readonly acc:IAccount;
    readonly bet:IBet;
    readonly pick:AVAILABLE_PICK;
}

export interface IStepGame {
    readonly ID:number;
    readonly bet:IBet;
    readonly accountPick:AVAILABLE_PICK;
    readonly result:IResult;
}