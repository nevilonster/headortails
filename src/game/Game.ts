import {AVAILABLE_PICK, COIN_SIDE, IAccountPick, IBet, IGame, IStepGame} from "./IGame";
import {IMathModel, IResult, MathData} from "../logic/MathTypes";
import {MATH_ERROR, MathErrors, RollBackError} from "../logic/MathErrors";
import {ErrorResult} from "../logic/results/ErrorResult";
import {StatisticModule} from "../statistic/StatisticModule";
import {GAME_ERROR, GameError} from "./GameError";

class GameStep implements IStepGame{
    readonly ID: number;
    readonly accountPick: AVAILABLE_PICK;
    readonly bet: IBet;
    readonly result: IResult;

    constructor(id:number, accountPick:AVAILABLE_PICK, bet:IBet, result:IResult) {
        this.ID = id;
        this.accountPick = accountPick;
        this.bet = bet;
        this.result = result;
    }
}

export class Game implements IGame{
    private _currentMathModel:IMathModel;
    private _steps:Array<IStepGame> = [];
    constructor() {
    }

    private clearCurrentModel():void{
        if(this._currentMathModel){
            this._currentMathModel.destroy();
        }
        this._currentMathModel = null;
    }

    setMathModel(model:IMathModel):void{
        if(this._currentMathModel && this._currentMathModel.ID === model.ID){
            console.log(`This model was set already.`)
            return;
        }
        if(model){
            this.clearCurrentModel();
            this._currentMathModel = model;
            return;
        }
    }

    exec(data:IAccountPick):IResult{
        if(this._currentMathModel){
            if(data.pick === COIN_SIDE.HEAD || data.pick === COIN_SIDE.TAIL){
                data.acc.spend(data.bet);
                const res:IResult = this._currentMathModel.exec(new MathData(data.bet, data.pick));

                const step:IStepGame = new GameStep(this._steps.length, data.pick, data.bet, res);
                this._steps.push(step);

                StatisticModule.getInstance().add(step);
                if(res instanceof ErrorResult){
                    throw new RollBackError(MATH_ERROR.ROLLBACK_ERROR, data.bet, data.acc);
                }

                data.acc.addBalance(res.getResult());
                return res;
            }
            throw new GameError(
                GAME_ERROR.GAME_PICK_ERROR,
                `${data.pick} unavailable. please use ${COIN_SIDE.TAIL} or ${COIN_SIDE.HEAD}`
            )
        }

        throw new MathErrors(MATH_ERROR.MATH_UNDEFINED, "Please set correct MathLibrary")
    }

    clear(): void {
        this.clearCurrentModel();
        this._steps = [];
    }

    log():void{
        StatisticModule.getInstance().getStatisticBySteps(this._steps);
    }
}