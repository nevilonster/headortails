import {BaseError} from "../errors/BaseError";

export enum GAME_ERROR {
    GAME_PICK_ERROR=1101
}

export class GameError extends BaseError{
    constructor(code:number, message:string) {
        super(code, message);
    }
}