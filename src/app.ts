import {GameError} from "./game/GameError";
import {InputModule} from "./input/InputModule";

const gameInput:InputModule = new InputModule();

gameInput.start();
