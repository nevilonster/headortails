export interface IBalanceAction{
    getBalance():number;
    addBalance(value:number):void;
    spend(value:number):void;
}

