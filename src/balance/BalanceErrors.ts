import {BaseError} from "../errors/BaseError";

export enum BALANCE_ERRORS_TYPE {
    INSUFFICIENT_FUNDS
}

export class BalanceError extends BaseError{
    constructor(code: BALANCE_ERRORS_TYPE, message: string) {
        super(code, message);
    }
}