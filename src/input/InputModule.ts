import {IAccount} from "../account/IAccount";
import {Account} from "../account/Account";
import {AVAILABLE_PICK, COIN_SIDE, IAccountPick, IBet, IGame} from "../game/IGame";
import {Game} from "../game/Game";
import readline, {Interface} from "readline";
import {IMathModel, IResult} from "../logic/MathTypes";
import {MathFiftyFiftyModel} from "../logic/models/MathFiftyFiftyModel";
import {GAME_ERROR, GameError} from "../game/GameError";
import {BalanceError} from "../balance/BalanceErrors";

const END_GAME_PARAM: string = "end";

export class InputModule{
    private _acc:IAccount;
    private _game:IGame;
    private _math:IMathModel;
    private _rl;

    constructor() {
        this._acc = new Account(1);
        this._game = new Game();
        this._math = new MathFiftyFiftyModel();
        this._game.setMathModel(this._math);

    }

    start(){
        this.printBlock("Start game")
        this._rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        this.addBalanceStep(this._acc);
    }

     printBlock(...args): void {
        console.log(`-----${args}----`)
    }

     print(...args): void {
        console.log(`${args}`)
    }

     endGameChecker(value: string): void {
        if (value === END_GAME_PARAM) {
            this.printBlock("Account");
            this.print("Balance:" + this._acc.getBalance());
            this.printBlock("Goodbay");
            this._rl.close();
            process.exit();
        }
    }

     addBalanceStep(acc: IAccount): void {
        this.printBlock("Add coins")
        this._rl.question("count:", (value) => {
            this.endGameChecker(value);

            const coins: number = Number(value);
            if (coins && coins > 0) {
                acc.addBalance(coins);
                this.choiseBetStep(acc);
            } else {
                this.print(`${value} is not accepted. Please write correct count`);
                this.addBalanceStep(acc);
            }
        })
    }

     continueGame(bet: IBet): void {
        this.printBlock("Do you want continue?");
        this._rl.question("y/n:", (value) => {
            this.endGameChecker(value);

            if (value === "y") {
                this.playGameStep(this._acc, bet);
            } else {
                this.endGameChecker(END_GAME_PARAM);
            }
        })
    }

     playAgain(acc: IAccount, pick: IAccountPick): void {
        this.printBlock("Repeat?");
        this._rl.question("y/n:", (value) => {
            this.endGameChecker(value);
            if (value === "y") {
                this.print(`you choise: ${pick.pick} bet:${pick.bet}`);
                let res:IResult | null = this.gameExec(pick);
                if(res === null){
                    return;
                }
                console.log(pick);
                console.log(res);
                this.playAgain(acc, pick);
            } else {
                this.continueGame(pick.bet);
            }
        })
    }

     choiseBetStep(acc: IAccount): void {
        this.printBlock("Choise bet");

        this._rl.question("count:", (value) => {
            this.endGameChecker(value);

            const bet: number = Number(value);
            if (bet && bet > 0) {
                this.playGameStep(acc, bet);
            } else {
                this.print(`Incorrect values. Please input valid number`);
                this.choiseBetStep(acc);
            }
        })
    }

     playGameStep(acc: IAccount, bet: IBet): void {
        this.printBlock("Please pick side:" + `${COIN_SIDE.HEAD} or ${COIN_SIDE.TAIL}`);
        this._rl.question("count:", (value) => {
            this.endGameChecker(value);
            //our sides is UpCamelCase
            value = value.toUpperCase();

            const pickValue: AVAILABLE_PICK = value as AVAILABLE_PICK;
            const accPick: IAccountPick = {acc: acc, pick: pickValue, bet: bet};
            let res:IResult | null = this.gameExec(accPick);
            if(res === null){
                return;
            }

            console.log(accPick);
            console.log(res);
            this.print(res.getMessage());
            this.playAgain(acc, accPick);
        })
    }

    //Should handle all errors and move to correct step...
    private gameExec(pick:IAccountPick):IResult{
        try {
            return this._game.exec(pick);
        }catch (e) {
            if(e instanceof GameError){
                console.log("Game Error", e.code);
                this.choiseBetStep(this._acc);
            }
            if(e instanceof BalanceError){
                console.log(`BALANCE ERROR, current balance:${this._acc.getBalance()}, bet: ${pick.bet}`);
                this.addBalanceStep(this._acc)
            }
            //TODO add all available errors
            return null;
        }
    }

     printInfo() {

    }
}

