import {BaseError} from "../errors/BaseError";
import {IAccount} from "../account/IAccount";

export enum MATH_ERROR {
    MATH_ALREADY_SET=1201,
    MATH_UNAVAILABLE=1202,
    MATH_UNDEFINED=1298,
    MATH_MANAGER_SET=1299,
    ROLLBACK_ERROR=12111
}

export class MathErrors extends BaseError{
    constructor(code:number, message?:string) {
        super(code, message);
    }
}

//If should return money to user
export class RollBackError extends BaseError{
    readonly bet:number;
    readonly acc:IAccount;
    constructor(code:number, bet:number, acc:IAccount, message?:string) {
        super(code, message);
        this.bet = bet;
        this.acc = acc;
    }
}