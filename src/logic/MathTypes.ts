import {AVAILABLE_PICK, IBet} from "../game/IGame";

export enum AVAILABLE_MATHES {
    FIFTY_FIFTY=1
}

export interface IResult {
    getResult():number | IBet;
    getMessage():string;
}

export interface IMathData {
    readonly bet:IBet,
    readonly pick:AVAILABLE_PICK,
}

export class MathData{
    readonly bet:IBet;
    readonly pick:AVAILABLE_PICK;
    constructor(bet:IBet, pick:AVAILABLE_PICK) {
        this.bet = bet;
        this.pick = pick;
    }
}

export interface IWinResult extends IResult{
    readonly winValue:number;
}

export interface ILoseResult extends IResult{

}

export interface IDrawResult extends IResult{

}

export interface IErrorResult extends IResult{

}

export interface IMathModel {
    readonly ID:number;

    exec(data:IMathData):IResult;
    destroy():void;
    toString():string;
}

