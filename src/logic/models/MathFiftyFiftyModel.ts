import {AVAILABLE_MATHES, IMathData, IMathModel, IResult} from "../MathTypes";
import {AVAILABLE_PICK, COIN_SIDE} from "../../game/IGame";
import {WinResult} from "../results/WinResult";
import {LoseResult} from "../results/LoseResult";

export class MathFiftyFiftyModel implements IMathModel{
    readonly ID: number;
    private _combination:Array<COIN_SIDE> = new Array<COIN_SIDE>(15);

    constructor() {
        this.ID = AVAILABLE_MATHES.FIFTY_FIFTY;
        //init combinations
        this._combination.fill(COIN_SIDE.HEAD, 0, 7);
        this._combination.fill(COIN_SIDE.TAIL, 7, 14);
        this._combination.fill(COIN_SIDE.EDGE, 14);
    }

    exec(data:IMathData): IResult {
        const v:number = Math.round(Math.random()*14);

        if(this._combination[v] === data.pick){
            return new WinResult(data.bet, this._combination[v]);
        }else{
            return new LoseResult(data.bet, this._combination[v]);
        }
    }

    destroy(): void {
        this._combination = null;
    }
}