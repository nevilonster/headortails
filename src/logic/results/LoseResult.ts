import {ILoseResult} from "../MathTypes";
import {AVAILABLE_PICK, IBet, COIN_SIDE} from "../../game/IGame";

export class LoseResult implements ILoseResult{
    private readonly _bet:IBet;
    readonly side:COIN_SIDE;
    constructor(bet:IBet, side:COIN_SIDE) {
        this._bet = bet;
        this.side = side;
    }

    getResult(): number | IBet {
        return 0;
    }

    getMessage(): string {
        return `Side: ${this.side}, You lose ${this._bet} coins`;
    }
}