import {IErrorResult} from "../MathTypes";
import {IBet} from "../../game/IGame";

export class ErrorResult implements IErrorResult{
    readonly ID: number;
    constructor() {
    }

    getResult(): number | IBet {
        return 0;
    }

    getMessage(): string {
        return "";
    }
}