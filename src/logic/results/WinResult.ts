import {IResult} from "../MathTypes";
import {AVAILABLE_PICK, IBet, COIN_SIDE} from "../../game/IGame";

export class WinResult implements IResult{
    readonly winValue:number;
    readonly side:COIN_SIDE;

    private readonly _curBet:IBet;
    constructor(bet:IBet, side:COIN_SIDE) {
        this._curBet = bet;
        this.winValue = bet * 2;
        this.side = side;
    }

    getResult(): number | IBet {
        return this.winValue;
    }

    getMessage():string{
        return `Side: ${this.side}, You Win ${this.getResult()} coins`
    }
}